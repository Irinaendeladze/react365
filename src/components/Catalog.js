import SingleProduct from "./SingleProduct";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { setProducts } from "./redax/actions/productActions";
import useStyles from "./style";
import React from "react";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import { useHistory } from "react-router";
import { getProducts } from "./API";
import EditProduct from "./EditProduct";

const Catalog = ({ handleOpen, onChecked }) => {
  const classes = useStyles();
  const products = useSelector((state) => state.allProducts.products);
  const dispatch = useDispatch();
  const history = useHistory();

  const [isOpenProductModal, setIsOpenProductModal] = useState(false);
  const [editingProductId, setEditingProductId] = useState(null);

  useEffect(() => {
    getProducts().then((results) => {
      dispatch(setProducts(results));
    });
  }, []);

  return (
    <main>
      <Grid className={classes.grid} container justify="center" spacing={4}>
        {products.map((item) => (
          <Grid item key={item.id} xs={12} sm={6} md={4}>
            <Link style={{ textDecoration: "none" }} to={`/catalog/${item.id}`}>
              <SingleProduct
                item={item}
                handleOpen={handleOpen}
                key={item.id}
                id={item.id}
                price={item.price}
                title={item.title}
                image={item.imageUrl}
                imageUrl={item.imageUrl}
                description={item.description}
                isChecked={item.isChecked}
                qty={item.qty}
                onChecked={() => onChecked(item.id)}
                onEdit={setEditingProductId}
              />
            </Link>
          </Grid>
        ))}
      </Grid>

      {(isOpenProductModal || editingProductId) && (
        <EditProduct
          isOpen={true}
          onClose={() => {
            setIsOpenProductModal(false);
            setEditingProductId(null);
            history.push("/catalog");
          }}
          productId={editingProductId}
        />
      )}
    </main>
  );
};
export default Catalog;
