const ClearButton = ({ handleClearAll }) => {
  return (
    <button className="clearSelected-button" onClick={handleClearAll}>
      Clear Selected
    </button>
  );
};

export default ClearButton;
