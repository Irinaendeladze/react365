import { VscSearch } from "react-icons/vsc";
import { VscQuestion } from "react-icons/vsc";
import { useState } from "react";
import ClearButton from "./ClearButton";
import SelectAllButton from "./SelectAll";
import { Grid } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";

const HeaderSearch = ({
  handleClearAll,
  handleSelectAll,
  selectedProducts,
}) => {
  const [searchInput, setSearchInput] = useState("");
  const products = useSelector((state) => state.allProducts.products);

  const dispatch = useDispatch();
  const filtered = JSON.parse(localStorage.getItem("products")).filter(
    (products) =>
      products.title.toLowerCase().includes(searchInput.toLowerCase())
  );

  const handleSearchClick = () => {
    dispatch({
      type: "SEARCH_INPUT",
      payload: filtered,
    });
  };

  const searchInputChanged = (event) => {
    setSearchInput(event.target.value);
  };

  return (
    <Grid>
      <div className="main__content">
        <div className="header-wrapper">
          <div className="nav__search">
            <div className="nav__search-left">
              <SelectAllButton handleSelectAll={handleSelectAll} />
              <span className="selected-title">{`selected ${selectedProducts} out of ${products.length} products`}</span>
              {selectedProducts > 0 ? (
                <ClearButton handleClearAll={handleClearAll} />
              ) : (
                ""
              )}
            </div>
            <div className="search">
              <input
                placeholder="search..."
                className="nav__search-input"
                type="text"
                id="searchInput"
                value={searchInput}
                onChange={searchInputChanged}
              />
              <button
                className="nav__search-button"
                onClick={handleSearchClick}
              >
                <VscSearch />
              </button>
              <div className="inventoryButton__question">
                <button className="inventoryButton">ADD TO INVENTORY</button>
                <VscQuestion size="32px" color="#49547d" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Grid>
  );
};
export default HeaderSearch;
