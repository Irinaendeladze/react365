const SelectAllButton = ({ handleSelectAll }) => {
  return (
    <button
      className="selecallButton"
      type="button"
      id="selectAll"
      onClick={handleSelectAll}
    >
      SELECT ALL
    </button>
  );
};

export default SelectAllButton;
