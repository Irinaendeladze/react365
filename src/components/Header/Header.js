import HeaderSearch from "./HeaderSearch";
import { MdSort } from "react-icons/md";
import useStyles from "../style";
import { useSelector, useDispatch } from "react-redux";
import { sortProducts } from "../redax/actions/productActions";
import { useState, useEffect } from "react";
import Modal from "@material-ui/core/Modal";
import * as yup from "yup";
import { addProduct, getSingleProduct } from "../API";
import { setProducts } from "../redax/actions/productActions";
import { useParams } from "react-router";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { toast } from "react-toastify";
import { notify } from "../Alert";
import { getProducts } from "../API";

const addProductValidation = yup.object().shape({
  title: yup.string().min(2).max(25),
  description: yup.string().min(10).max(255),
  price: yup.number().integer().min(50),
  imageUrl: yup.string().url(),
});
toast.configure();
const Header = ({ selectedProducts, handleClearAll, handleSelectAll }) => {
  const products = useSelector((state) => state.allProducts.products);
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const classes = useStyles();
  const { productId } = useParams();

  const handleSort = (event) => {
    dispatch(sortProducts(products, event.target.value));
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    if (productId) {
      getSingleProduct(productId).then((res) => {
        setProducts(res);
      });
    }
  }, [productId]);

  const handleSubmit = (values) => {
    addProduct(values).then((res) => {
      notify(toast.success("Product was added succesfully"));
      setOpen(false);
      getProducts().then((res) => {
        dispatch({
          type: "ADD_PRODUCT",
          payload: res,
        });
        localStorage.setItem("products", JSON.stringify(res));
      });
    });
  };

  const body = (
    <div className={classes.editAddPaper}>
      <Formik
        enableReinitialize
        initialValues={
          productId
            ? {
                title: products.title,
                description: products.description,
                price: products.price,
                imageUrl: products.imageUrl,
              }
            : {
                title: "",
                description: "",
                price: "",
                imageUrl: "",
              }
        }
        onSubmit={handleSubmit}
        validationSchema={addProductValidation}
      >
        <Form className={classes.addProduct}>
          <h4 className={classes.editAddProducTitle}>Add new product</h4>
          <Field
            placeholder="Title"
            name="title"
            className={classes.addProductField}
          />
          <ErrorMessage name="title" components="div" />
          <Field
            placeholder="Description"
            name="description"
            components="textarea"
            className={classes.addProductField}
          />
          <ErrorMessage name="description" components="div" />
          <Field
            placeholder="Price"
            name="price"
            className={classes.addProductField}
          />
          <ErrorMessage name="price" components="div" />
          <Field
            placeholder="ImageUrl"
            name="imageUrl"
            className={classes.addProductField}
          />
          <ErrorMessage
            name="imageUrl"
            components="div"
            className={classes.addProductField}
          />
          <input
            type="submit"
            value="add Product"
            className={classes.saveChangesBtn}
          />
        </Form>
      </Formik>
    </div>
  );
  return (
    <div>
      <HeaderSearch
        handleClearAll={handleClearAll}
        handleSelectAll={handleSelectAll}
        selectedProducts={selectedProducts}
      />
      <nav className="header__nav">
        <div className="nav__sort">
          <MdSort className="sort-icon" size="20px" />
          <select id="sort" className="sort__item" onChange={handleSort}>
            <option value="sort">Sort by :</option>
            <option value="default">New Arrivals</option>
            <option value="asc">Price: Low to High</option>
            <option value="desc">Price : High to Low</option>
            <option value="abc">Abc..</option>
            <option value="reverse-abc">Reverse Abc</option>
          </select>
        </div>

        <div>
          <button
            type="button"
            onClick={handleOpen}
            className={classes.addProductbtn}
          >
            ADD PRODUCT
          </button>
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            className={classes.editProductModal}
          >
            {body}
          </Modal>
        </div>
      </nav>
    </div>
  );
};
export default Header;
