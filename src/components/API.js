import axios from "axios";
import { toast } from "react-toastify";
import { notify } from "../components/Alert";

const SERVER_URL = "http://18.185.148.165:3000/";

const SERVER_URL_V1 = SERVER_URL + "api/v1/";
toast.configure();
axios.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

axios.interceptors.response.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

export const login = async (email, password) => {
  try {
    const results = await axios.post(SERVER_URL + "login", { email, password });
    // console.log(results.data.data);
    localStorage.setItem("user", JSON.stringify(results.data.data));
    localStorage.setItem("token", results.data.data.token);
  } catch (err) {
    throw new Error(err);
  }
};

export const cart = async () => {
  try {
    const results = await axios.get(SERVER_URL_V1 + "cart");
    return results.data.data;
    // console.log(results);
  } catch (err) {
    if (err.response.status === 401) {
      localStorage.removeItem("token");
      localStorage.removeItem("user");
      window.location.href = "/profile";
    }
  }
};
export const register = async (data) => {
  try {
    const results = await axios.post(SERVER_URL + "register", data);
    localStorage.setItem("user", JSON.stringify(results.data.data));
    localStorage.setItem("token", results.data.data.token);
    console.log(results.data.data);
    window.location = "/cart";
  } catch (err) {
    console.log(err);
  }
};

export const getProducts = async () => {
  const results = await axios.get(SERVER_URL_V1 + "products");
  localStorage.setItem("products", JSON.stringify(results.data.data));
  return results.data.data;
};

export const getSingleProduct = async (id) => {
  const results = await axios.get(SERVER_URL_V1 + `products/${id}`);
  return results.data.data;
};

export const addToCart = async (productId, qty) => {
  const results = await axios.post(SERVER_URL_V1 + "cart/add", {
    productId,
    qty,
  });

  notify(toast.success(`${qty} product add to cart`));
  return results.data.data;
};

export const deleteItem = async (id) => {
  try {
    const results = await axios.post(SERVER_URL_V1 + `cart/remove/${id}`);
    console.log(results.data);
  } catch (err) {
    console.log(err);
  }
};

export const addProduct = async (data) => {
  const results = await axios.post(SERVER_URL_V1 + "products", data);
  return results.data.data;
};

export const updateProduct = async (id, data) => {
  try {
    const results = await axios.put(SERVER_URL_V1 + `products/${id}`, data);
    return results.data.data;
  } catch (err) {
    console.log(err);
  }
};

export const updateCart = async (id, qty) => {
  const results = await axios.post(SERVER_URL_V1 + `cart/update/${id}`, {
    id,
    qty,
  });

  return results.data.data;
};
export const deleteProduct = async (id) => {
  try {
    const results = await axios.delete(SERVER_URL_V1 + `products/${id}`);
    return results.data.data;
  } catch (err) {
    return err;
  }
};
