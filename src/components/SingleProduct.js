import Checkboxs from "./Checkbox";
import useStyles from "./style";
import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import { toast } from "react-toastify";
import { notify } from "./Alert";
import { useState } from "react";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import {
  Card,
  CardMedia,
  CardContent,
  Typography,
  Button,
} from "@material-ui/core";
import { addToCart, deleteProduct, getProducts } from "./API";
import { useDispatch } from "react-redux";

toast.configure();
const SingleProduct = ({
  description,
  handleOpen,
  image,
  imageUrl,
  title,
  price,
  isChecked,
  onChecked,
  id,
  onEdit,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [qty, setQty] = useState(1);
  const removeAddedProduct = () => {
    if (id) {
      deleteProduct(id)
        .then((res) => {
          getProducts().then((res) => {
            dispatch({
              type: "DELETE_PRODUCT",
              payload: res,
            });
            notify(toast.success("Product deleted Successfully"));
            localStorage.setItem("products", JSON.stringify(res));
          });
        })
        .catch((err) => alert(err.message));
    }
  };
  const plusMinus = (e) => {
    if (e.target.value === "plus") {
      setQty(qty + 1);
    } else if (e.target.value === "minus") {
      setQty(qty - 1);
    }
  };

  return (
    <>
      <Card key={id} className={classes.root}>
        <div className={classes.cardHeader}>
          <Checkboxs isChecked={isChecked} onChecked={onChecked} />

          <Button
            variant="contained"
            color="primary"
            elevation={0}
            className={classes.inputBtn}
            onClick={() => {
              addToCart(id, qty);
            }}
          >
            Add To Inventory
          </Button>
        </div>

        <CardMedia
          onClick={() =>
            handleOpen({ imageUrl, price, title, description, id })
          }
          className={classes.media}
          image={image}
          id={id}
        />
        <div className={classes.productQty}>
          <button
            className={classes.increaseDecreaseBtn}
            value={"plus"}
            onClick={plusMinus}
          >
            +
          </button>
          <button className={classes.increaseDecreaseBtn}>{qty}</button>
          <button
            className={classes.increaseDecreaseBtn}
            value={"minus"}
            onClick={plusMinus}
            disabled={qty <= 1}
          >
            -
          </button>
        </div>
        <CardContent>
          <div className={classes.cardContent}>
            <Typography className="catalog__title" variant="h6">
              {title}
            </Typography>
            <div>
              <Typography className="catalog__title" variant="h6">
                By: <span className={classes.spanSupplier}>US Supplier166</span>
              </Typography>
            </div>
          </div>
          <Typography className="catalog__prices" variant="h6">
            <ul className={classes.priceSection}>
              <li className={classes.rrpPrice}>
                <div>
                  <IconButton aria-label="edit" color="primary">
                    <EditOutlinedIcon
                      aria-label="edit"
                      color="primary"
                      onClick={() => onEdit(id)}
                    />
                  </IconButton>
                </div>
              </li>
              <li className={classes.costPrice}>
                <strong>${price}</strong>
                <br />
                <span>COST</span>
              </li>
              <li className={classes.profitPrice}>
                <IconButton
                  aria-label="delete"
                  color="primary"
                  onClick={removeAddedProduct}
                >
                  <DeleteIcon />
                </IconButton>
              </li>
            </ul>
          </Typography>
        </CardContent>
      </Card>
    </>
  );
};
export default SingleProduct;
