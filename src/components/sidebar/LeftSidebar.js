import logo from "../icons/dropship_logo.png";
import { AiOutlineDashboard } from "react-icons/ai";
import { AiOutlineUnorderedList } from "react-icons/ai";
import { FiBox } from "react-icons/fi";
import { IoMdCart } from "react-icons/io";
import { AiFillCarryOut } from "react-icons/ai";
import { BiTransfer } from "react-icons/bi";
import { FaClipboardList } from "react-icons/fa";
import { Link } from "react-router-dom";
import React from "react";
import { CgProfile } from "react-icons/cg";
import { Hidden } from "@material-ui/core";

const Leftsidebar = () => {
  return (
    <Hidden smDown>
      <aside class="left__sidebar">
        <div class="left__sidebar-logo">
          <img src={logo} className="App-logo" alt="logo" width="50px" />
        </div>
        <ul>
          <li className="profile">
            <Link to="/Profile">
              <CgProfile size="32px" color="#49547d" />
            </Link>
          </li>
          <li className="dashboard">
            <Link to="/dashboard">
              {" "}
              <AiOutlineDashboard size="28px" color="#49547d" />{" "}
            </Link>
          </li>
          <li className="catalog">
            <Link to="/catalog">
              <AiOutlineUnorderedList size="25px" color="#49547d" />
            </Link>
          </li>
          <li className="inventory">
            <Link to="/inventory">
              <FiBox size="22px" color="#49547d" />
            </Link>
          </li>
          <li className="cart">
            <Link to="/cart">
              {" "}
              <IoMdCart size="22px" color="#49547d" />
            </Link>
          </li>
          <li className="orders">
            <Link to="/orders">
              <AiFillCarryOut size="22px" color="#49547d" />
            </Link>
          </li>
          <li className="transaction">
            <Link to="/transaction">
              <BiTransfer size="22px" color="#49547d" />
            </Link>
          </li>
          <li class="storesList">
            <Link to="/storesList">
              <FaClipboardList size="22px" color="#49547d" />
            </Link>
          </li>
          <li class="addProduct">
            <Link to="/getProducts">
              <FaClipboardList size="22px" color="#49547d" />
            </Link>
          </li>
        </ul>
      </aside>
    </Hidden>
  );
};
export default Leftsidebar;
