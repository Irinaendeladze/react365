import { useEffect, useState } from "react";
import axios from "axios";
import React from "react";

const Categories = ({ products, setProducts, allProducts, setAllProducts }) => {
  const [categories, setCategories] = useState([]);
  const [categoryValue, setCategoryValue] = useState("");

  useEffect(() => {
    axios.get("https://fakestoreapi.com/products/categories").then((result) => {
      localStorage.setItem("categories", JSON.stringify(result));
      setCategories(result.data);
      if (categoryValue) {
        const CategoriesList = allProducts.filter(
          (item) => item.category === categoryValue
        );

        setAllProducts(CategoriesList);
      }
    });
  }, [categoryValue]);

  const handleCategory = (event) => {
    setCategoryValue(event.target.value);
  };

  console.log(allProducts);
  return (
    <>
      <div>
        <select className="subMenu__dropdownbtn" onChange={handleCategory}>
          <option value="default">Choose Category</option>
          {categories.map((item) => {
            return <option value={item}>{item}</option>;
          })}
        </select>
      </div>
    </>
  );
};

export default Categories;
