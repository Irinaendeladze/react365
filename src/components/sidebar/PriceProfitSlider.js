import { useState } from "react";
import useStyles from "../style";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import React from "react";

const PriceProfitslider = (allProducts) => {
  const [priceSlider, setPriceSlider] = useState([1, 1500]);
  const [profitSlider, setProfitSlider] = useState([1, 1500]);

  const handlePriceSlider = (event, data) => {
    setPriceSlider(data);
  };
  const handleProfitSlider = (event, data) => {
    setProfitSlider(data);
    console.log(data);
  };
  const classes = useStyles();

  return (
    <div className="range-frame price-range">
      <div className={classes.slider}>
        <Typography id="range-slider" gutterBottom>
          PRICE RANGE
        </Typography>
        <Slider
          min={1}
          max={1500}
          step={1}
          value={priceSlider}
          onChange={handlePriceSlider}
          valueLabelDisplay="auto"
          aria-labelledby="range-slider"
        />
        <div className={classes.rangeWrapper}>
          <div className={classes.priceNumber}>
            <span className={classes.simbol}>$</span>
            <span className={classes.number}>{priceSlider[0]}</span>
          </div>
          <div className={classes.priceNumber}>
            <span className={classes.simbol}>$</span>
            <span className={classes.number}>{priceSlider[1]}</span>
          </div>
        </div>
      </div>

      <div className={classes.slider}>
        <Typography id="range-slider" gutterBottom>
          PROFIT RANGE
        </Typography>
        <Slider
          min={1}
          max={1500}
          step={1}
          value={profitSlider}
          onChange={handleProfitSlider}
          valueLabelDisplay="auto"
          aria-labelledby="range-slider"
        />
        <div className={classes.rangeWrapper}>
          <div className={classes.priceNumber}>
            <span className={classes.simbol}>%</span>
            <span className={classes.number}>{profitSlider[0]}</span>
          </div>
          <div className={classes.priceNumber}>
            <span className={classes.simbol}>%</span>
            <span className={classes.number}>{profitSlider[1]}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PriceProfitslider;
