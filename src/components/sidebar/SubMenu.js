import { Hidden } from "@material-ui/core";
import Categories from "./Categories";
import PriceProfitslider from "./PriceProfitSlider";

const SubMenu = ({ products, setProducts, allProducts, setAllProducts }) => {
  return (
    <Hidden smDown>
      <nav className="right__sidebar">
        <div className="subMenu">
          <button className="subMenu__dropbtn">Choose Niche</button>

          <Categories
            products={products}
            setProducts={setProducts}
            allProducts={allProducts}
            setAllProducts={setAllProducts}
          />
          <select className="subMenu__shipFrom">
            <option value="ship">Ship From </option>
            <option value="Australia">Australia </option>
            <option value="chine"> China </option>
            <option value="price">Czech Republic</option>
            <option value="profit">France </option>
            <option value="profit">Germany</option>
          </select>
          <select className="subMenu__shipTo">
            <option value="ship">Ship To </option>
            <option value="Australia">Australia </option>
            <option value="chine"> China </option>
            <option value="price">Czech Republic</option>
            <option value="profit">France </option>
            <option value="profit">Germany</option>
          </select>
          <select className="subMenu__selectSupplier">
            <option value="ship">Select Supplier </option>
            <option value="Australia">Australia </option>
            <option value="chine"> China </option>
            <option value="price">Czech Republic</option>
            <option value="profit">France </option>
            <option value="profit">Germany</option>
          </select>
          <PriceProfitslider allProducts={allProducts} />
          <button className="subMenu__resetfilter">RESET FILTER</button>
        </div>
      </nav>
    </Hidden>
  );
};

export default SubMenu;
