import { ActionTypes } from "../action-types";

const initialState = {
  products: [],
  selectedProducts: [],
  modalOpen: false,
};

export const productReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_PRODUCTS:
      return { ...state, products: payload };
    case ActionTypes.SORT_PRODUCTS:
      return { ...state, products: payload };
    case ActionTypes.SEARCH_INPUT:
      return {
        ...state,
        products: payload,
      };
    case ActionTypes.SELECTED_PRODUCTS:
      return {
        ...state,
        selectedProducts: payload,
      };
    case ActionTypes.HANDLE_SELECT_ALL:
      return {
        ...state,
        products: payload,
      };
    case ActionTypes.HANDLE_CLEAR_ALL:
      return {
        ...state,
        products: payload,
      };
    case ActionTypes.EDIT_PRODUCT:
      return {
        ...state,
        products: payload,
      };
    case ActionTypes.DELETE_PRODUCT:
      return {
        ...state,
        products: payload,
      };
    case ActionTypes.ADD_PRODUCT:
      return {
        ...state,
        products: payload,
      };
    default:
      return state;
  }
};
