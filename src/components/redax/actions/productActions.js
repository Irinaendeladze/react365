import { ActionTypes } from "../contants/action-types";

export const setProducts = (products) => {
  return {
    type: ActionTypes.SET_PRODUCTS,
    payload: products,
  };
};

export const sortProducts = (products, filterArr) => {
  let sortedData = [...products];
  switch (filterArr) {
    case "default":
      sortedData = sortedData.sort((a, b) => b.createAt - a.createAt);
    case "asc":
      sortedData = sortedData.sort((a, b) => a.price - b.price);
      break;
    case "desc":
      sortedData = sortedData.sort((a, b) => b.price - a.price);
      break;
    case "abc":
      sortedData = sortedData.sort((a, b) => a.title.localeCompare(b.title));
      break;
    case "reverse-abc":
      sortedData = sortedData.sort((a, b) => b.title.localeCompare(a.title));
      break;
    default:
      sortedData = [];
  }
  return {
    type: ActionTypes.SORT_PRODUCTS,
    payload: sortedData,
  };
};

export const searchProducts = (searchValue) => {
  return {
    type: ActionTypes.SEARCH_INPUT,
    payload: searchValue,
  };
};

export const selectedProducts = (productId) => {
  return {
    type: ActionTypes.SELECTED_PRODUCTS,
    payload: productId,
  };
};

export const handleSelectAll = () => {
  return {
    type: ActionTypes.HANDLE_SELECT_ALL,
  };
};

export const handleClearAll = () => {
  return {
    type: ActionTypes.HANDLE_CLEAR_ALL,
  };
};
export const editProduct = () => {
  return {
    type: ActionTypes.EDIT_PRODUCT,
  };
};
export const deleteProduct = () => {
  return {
    type: ActionTypes.DELETE_PRODUCT,
  };
};

export const addProduct = () => {
  return {
    type: ActionTypes.ADD_PRODUCT,
  };
};
