import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export const notify = () => {
  toast.success({
    position: toast.POSITION.TOP_CENTER,
    autoClose: 1000,
  });
};
