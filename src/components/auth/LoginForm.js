import { useFormik } from "formik";
import logo from "../icons/dropship_logo.png";
import useStyles from "../style";
import React, { useContext, useState } from "react";
import Link from "@material-ui/core/Link";
import { Marginer } from "./Marginer";
import Box from "@material-ui/core/Box";
import { AccountContext } from "./context";
import * as yup from "yup";
import axios from "axios";

const validationSchema = yup.object({
  email: yup.string().required(),
  password: yup.string().required(),
});

export function LoginForm(props) {
  const { switchToSignup } = useContext(AccountContext);
  const [error, setError] = useState(null);

  const onSubmit = async (values) => {
    try {
      setError(null);
      const results = await axios.post(
        "http://18.185.148.165:3000/login",
        values
      );
      localStorage.setItem("user", JSON.stringify(results.data.data));
      localStorage.setItem("token", results.data.data.token);
      console.log(results.data.data);
      window.location = "/cart";
    } catch (err) {
      alert("password or surname is incorrect");
    }
  };

  const formik = useFormik({
    initialValues: { email: "", password: "" },
    validateOnBlur: true,
    onSubmit,
    validationSchema: validationSchema,
  });
  const classes = useStyles();
  return (
    <Box className={classes.boxContainer}>
      <div className={classes.authLogo}>
        <img
          src={logo}
          className="App-logo"
          alt="logo"
          width="50px"
          height="40px"
        />
        <h3>Members Log In</h3>
      </div>
      <div className={classes.FormError}>{error ? error : ""}</div>
      <form className={classes.formContainer} onSubmit={formik.handleSubmit}>
        <form className={classes.fieldContainer}>
          <input
            className={classes.FormInput}
            name="email"
            placeholder="Email"
            value={formik.values.email}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {
            <div className={classes.FieldError}>
              {formik.touched.email && formik.errors.email
                ? formik.errors.email
                : ""}
            </div>
          }
        </form>
        <form className={classes.fieldContainer}>
          <input
            className={classes.FormInput}
            name="password"
            type="password"
            placeholder="Password"
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {
            <div className={classes.FieldError}>
              {formik.touched.password && formik.errors.password
                ? formik.errors.password
                : ""}
            </div>
          }
        </form>
        <div className={classes.mutedLink} href="#">
          Forgot Password?
        </div>
        <Marginer direction="vertical" margin="1em" />
        <button
          className={classes.SubmitButton}
          type="submit"
          disabled={!formik.isValid}
        >
          Login
        </button>
      </form>
      <Marginer direction="vertical" margin={5} />
      <div className={classes.mutedLink} href="#">
        Dont have an Account?
        <Link className={classes.boldLink} to="#" onClick={switchToSignup}>
          sign up
        </Link>
      </div>
    </Box>
  );
}
