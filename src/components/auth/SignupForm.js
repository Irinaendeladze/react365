import { useFormik } from "formik";
import React, { useContext, useState } from "react";
import logo from "../icons/dropship_logo.png";
import useStyles from "../style";
import { Marginer } from "./Marginer";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import { AccountContext } from "./context";
import * as yup from "yup";
import axios from "axios";

const PASSWORD_REGEX = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
axios.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

const validationSchema = yup.object({
  firstName: yup
    .string()
    .max(15, "Must be 15 characters or less")
    .required("Required"),
  lastName: yup
    .string()
    .max(20, "Must be 20 characters or less")
    .required("Required"),
  email: yup.string().email("Email is invalid").required("Email is required"),
  password: yup
    .string()
    .matches(PASSWORD_REGEX, "Password must be at least 6 charaters")
    .required("Password is required"),
  passwordConfirmation: yup
    .string()
    .oneOf([yup.ref("password"), null], "Password must match")
    .required("Confirm password is required"),
});

export function SignupForm(props) {
  const { switchToSignin } = useContext(AccountContext);
  const [success, setSuccess] = useState();
  const [error, setError] = useState(null);

  const onSubmit = async (value) => {
    try {
      const results = await axios.post(
        "http://18.185.148.165:3000/register",
        value
      );
      localStorage.setItem("user", JSON.stringify(results.data.data));
      localStorage.setItem("token", results.data.data.token);
      console.log(results.data.data);
      window.location = "/catalog";
    } catch (err) {
      alert("wrong");
    }
  };

  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      passwordConfirmation: "",
    },
    validateOnBlur: true,
    onSubmit,
    validationSchema: validationSchema,
  });

  console.log("Error", error);
  const classes = useStyles();
  return (
    <Box className={classes.boxContainer}>
      <div className={classes.authLogo}>
        <img
          src={logo}
          className="App-logo"
          alt="logo"
          width="50px"
          height="40px"
        />
        <h3>Sign Up</h3>
      </div>
      {!error && (
        <div className={classes.FormSuccess}>{success ? success : ""}</div>
      )}
      {!success && (
        <div className={classes.FormError}>{error ? error : ""}</div>
      )}
      <form className={classes.formContainer} onSubmit={formik.handleSubmit}>
        <form className={classes.fieldContainer}>
          <input
            className={classes.FormInput}
            name="firstName"
            placeholder="First Name"
            value={formik.values.firstName}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <div className={classes.FieldError}>
            {formik.touched.firstName && formik.errors.firstName
              ? formik.errors.firstName
              : ""}
          </div>
        </form>
        <form className={classes.fieldContainer}>
          <input
            className={classes.FormInput}
            name="lastName"
            placeholder="Last Name"
            value={formik.values.lastName}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <div className={classes.FieldError}>
            {formik.touched.lastName && formik.errors.lastName
              ? formik.errors.lastName
              : ""}
          </div>
        </form>
        <form className={classes.fieldContainer}>
          <input
            className={classes.FormInput}
            name="email"
            placeholder="Email"
            value={formik.values.email}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <div className={classes.FieldError}>
            {formik.touched.email && formik.errors.email
              ? formik.errors.email
              : ""}
          </div>
        </form>
        <form className={classes.fieldContainer}>
          <input
            className={classes.FormInput}
            name="password"
            type="password"
            placeholder="Password"
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <div className={classes.FieldError}>
            {formik.touched.password && formik.errors.password
              ? formik.errors.password
              : ""}
          </div>
        </form>
        <form className={classes.fieldContainer}>
          <input
            className={classes.FormInput}
            name="passwordConfirmation"
            type="password"
            placeholder="password Confirmation"
            value={formik.values.passwordConfirmation}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <div className={classes.FieldError}>
            {formik.touched.passwordConfirmation &&
            formik.errors.passwordConfirmation
              ? formik.errors.passwordConfirmation
              : ""}
          </div>
        </form>
        <Marginer direction="vertical" margin="16px" />
        <button
          className={classes.SubmitButton}
          type="submit"
          disabled={!formik.isValid}
        >
          Signup
        </button>
      </form>
      <Marginer direction="vertical" margin={5} />
      <div className={classes.mutedLink} href="#">
        Already have an account?
        <Link to="/signin" onClick={switchToSignin}>
          sign in
        </Link>
      </div>
    </Box>
  );
}
