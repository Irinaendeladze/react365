import { Formik, Form, Field, ErrorMessage } from "formik";
import {
  addProduct,
  deleteProduct,
  getSingleProduct,
  updateProduct,
} from "../API";
import useStyles from "../style";
import * as yup from "yup";
import { useParams } from "react-router";
import { useEffect, useState } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { Button } from "@material-ui/core";

const addProductValidation = yup.object().shape({
  title: yup.string().min(2).max(25),
  description: yup.string().min(10).max(255),
  price: yup.number().integer().min(50),
  imageUrl: yup.string().url(),
});

const AddProduct = () => {
  const classes = useStyles();
  const { productId } = useParams();

  const [product, setProduct] = useState({});

  useEffect(() => {
    if (productId) {
      getSingleProduct(productId).then((res) => {
        setProduct(res);
      });
    }
  }, [productId]);

  const handleSubmit = (values) => {
    if (productId) {
      updateProduct(productId, values)
        .then((res) => {
          alert("update Successful");
        })
        .catch((err) => alert(err.message));
    } else {
      addProduct(values).then((res) => {
        alert("Product was added succesfully");
      });
    }
  };

  const removeAddedProduct = () => {
    if (productId) {
      deleteProduct(productId)
        .then((res) => {
          alert("delete Successful");
        })
        .catch((err) => alert(err.message));
    }
  };
  return (
    <>
      <div className={classes.root}>
        <AppBar position="static" elevation={0}>
          <Toolbar className={classes.Toolbar}>
            <Typography variant="h6" className={classes.title}>
              <p>{productId ? "Edit" : "Add"} Product</p>
            </Typography>
          </Toolbar>
        </AppBar>
        <Formik
          enableReinitialize
          initialValues={
            productId
              ? {
                  title: product.title,
                  description: product.description,
                  price: product.price,
                  imageUrl: product.imageUrl,
                }
              : {
                  title: "",
                  description: "",
                  price: "",
                  imageUrl: "",
                }
          }
          onSubmit={handleSubmit}
          validationSchema={addProductValidation}
        >
          <Form className={classes.addProduct}>
            <Field placeholder="Title" name="title" />
            <ErrorMessage name="title" components="div" />
            <Field
              placeholder="Description"
              name="description"
              components="textarea"
            />
            <ErrorMessage name="description" components="div" />
            <Field placeholder="Price" name="price" />
            <ErrorMessage name="price" components="div" />
            <Field placeholder="ImageUrl" name="imageUrl" />
            <ErrorMessage name="imageUrl" components="div" />
            <input type="submit" value="Save" />
            <Button color="secondary" onClick={removeAddedProduct}>
              {" "}
              Delete
            </Button>
          </Form>
        </Formik>
      </div>
    </>
  );
};

export default AddProduct;
