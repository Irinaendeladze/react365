import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Drawer from "@material-ui/core/Drawer";
import Catalog from "../Catalog";

import IconButton from "@material-ui/core/IconButton";
import LeftSidebar from "../sidebar/LeftSidebar";

import useStyles from "../style";
import React from "react";
import Logout from "./Logout";

const Profile = () => {
  const classes = useStyles();
  return (
    <>
      <div className={classes.root}>
        <AppBar
          position="fixed"
          elevation={0}
          className={classes.profileHeader}
        >
          <Toolbar className={classes.Toolbar}>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            ></IconButton>
            <Typography variant="h6" className={classes.title}>
              MY PROFILE
            </Typography>
            <Logout />
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawerWidth}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}
          anchor="left"
        >
          <LeftSidebar />
        </Drawer>{" "}
        <div className={classes.subProfile}>
          <form className={classes.profileForm}>
            <div className={classes.avatar}>
              <div className={classes.avatarHeading}>
                <nav>
                  <ul className={classes.subNav}>
                    <li>PROFILE</li>
                    <li>BILLING</li>
                    <li>INVOICE HISTORY</li>
                  </ul>
                </nav>
                <div className={classes.profileBtn}>
                  <button className={classes.deactivateBtn}>
                    {" "}
                    DEACTIVATE ACCOUNT
                  </button>
                </div>
              </div>
              <div className={classes.avatarWrapper}>
                <div className={classes.avatarWrapperPosition}>
                  <div className={classes.avatarWrapperContent}>
                    <p className={classes.avatarProfileTitle}>
                      PROFILE PICTURE
                    </p>
                    <div className={classes.avatarCont}>
                      <img
                        src="https://img.favpng.com/25/7/23/computer-icons-user-profile-avatar-image-png-favpng-LFqDyLRhe3PBXM0sx2LufsGFU.jpg"
                        alt="profile"
                        width="250px"
                      />
                    </div>
                    <input
                      type="file"
                      name="user-image"
                      id="upload-img"
                      className={classes.avatarInput}
                    />
                    <label
                      htmlFor="upload-img"
                      id="upload-img"
                      className={classes.avatarLabel}
                    >
                      {" "}
                      UPLOAD
                    </label>
                  </div>
                  <div className={classes.userPassword}>
                    <p className={classes.avatarProfileTitle}>
                      {" "}
                      CHANGE PASSWORD
                    </p>
                    <div className={classes.passwordReset}>
                      <form noValidate className={classes.passwordForm}>
                        <div className={classes.passwordCurrent}>
                          <label htmlFor="current-password">
                            Current Password
                          </label>
                          <div className={classes.passwordInputCont}>
                            <input
                              formcontrolname="currentPassword"
                              id="current-password"
                              name="current-password"
                              placeholder=""
                              required=""
                              type="password"
                              className={classes.profileInput}
                            ></input>
                          </div>
                        </div>
                        <div className={classes.passwordNew}>
                          <label htmlFor="new-password">New Password</label>
                          <div className={classes.passwordInputCont}>
                            <input
                              formcontrolname="newPassword"
                              id="new_password"
                              name="new_password"
                              placeholder=""
                              type="password"
                              className={classes.profileInput}
                            />
                          </div>
                        </div>
                        <div className={classes.passwordConfirm}>
                          <label htmlFor="confirm-password">
                            Confirm new password
                          </label>
                          <div className={classes.passwordInputCont}>
                            <input
                              formcontrolname="confirmPassword"
                              id="confirm-password"
                              name="confirm-password"
                              placeholder=""
                              required=""
                              type="password"
                              className={classes.profileInput}
                            />
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div className={classes.profileDetails}>
                    <div>
                      <div className={classes.profileMain}>
                        <p className={classes.avatarProfileTitle}>
                          {" "}
                          PERSONAL DETAIL
                        </p>
                        <div className={classes.deitailInputContent}>
                          <label for="first_name">First Name</label>
                          <input
                            formcontrolname="firstName"
                            id="first_name"
                            name="first_name"
                            placeholder="First Name"
                            type="text"
                            className={classes.userInputInfo}
                          ></input>
                        </div>
                        <div className={classes.deitailInputContent}>
                          <label for="last_name">last name</label>
                          <input
                            formcontrolname="lastName"
                            id="last_name"
                            name="last_name"
                            placeholder="Last Name"
                            type="text"
                            className={classes.userInputInfo}
                          ></input>
                        </div>
                        <div className={classes.deitailInputContent}>
                          <label for="last_name">Country</label>
                          <input
                            formcontrolname="country"
                            id="country-box"
                            placeholder="Select Country"
                            type="text"
                            className={classes.userInputInfo}
                          ></input>
                        </div>
                      </div>

                      <p className={classes.avatarProfileRight}>
                        {" "}
                        CONTACT INFORMATION
                      </p>
                      <div className={classes.profileMainRight}>
                        <div className={classes.detailsInputCont}>
                          <label for="first_name">First name</label>
                          <input
                            formcontrolname="firstName"
                            id="first_name"
                            name="first_name"
                            placeholder="First Name"
                            type="text"
                            className={classes.userInputInfo}
                          ></input>
                        </div>
                        <div className={classes.detailsInputCont}>
                          <label for="email">Email</label>
                          <input
                            formcontrolname="email"
                            id="email"
                            name="email"
                            placeholder="Email"
                            required
                            type="email"
                            className={classes.userInputInfo}
                          ></input>
                        </div>
                        <div className={classes.detailsInputCont}>
                          <label for="Skype">Skype</label>
                          <input
                            formcontrolname="Skype"
                            id="Skype"
                            placeholder="Skype"
                            type="text"
                            className={classes.userInputInfo}
                          ></input>
                        </div>
                        <div className={classes.detailsInputCont}>
                          <label for="Phone">Phone</label>
                          <input
                            formcontrolname="Phone"
                            id="Phone"
                            placeholder="Phone"
                            type="number"
                            className={classes.userInputInfo}
                          ></input>
                        </div>
                      </div>
                    </div>
                    <button
                      className={classes.profileSubmit}
                      type="submit"
                      id="change-profile"
                    >
                      Save Changes
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default Profile;
