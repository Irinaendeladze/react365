import useStyles from "../style";
import { useState, useRef } from "react";
import { LoginForm } from "../auth/LoginForm";
import { AccountContext } from "../auth/context";
import { SignupForm } from "../auth/SignupForm";
import Modal from "@material-ui/core/Modal";
import React from "react";

const expandingTransition = {
  type: "spring",
  duration: 2.3,
  stiffness: 30,
};

export default function FirstPage(props) {
  const [isExpanded, setExpanded] = useState(false);
  const [active, setActive] = useState("signin");
  const classes = useStyles();
  const rootRef = useRef(null);
  const playExpandingEffect = () => {
    setExpanded(true);

    setTimeout(() => {
      setExpanded(false);
    }, expandingTransition.duration * 1000 - 1500);
  };

  const switchActive = (active) => {
    setTimeout(() => setActive(active), 400);
  };

  const switchToSignup = () => {
    playExpandingEffect();
    switchActive("signup");
  };

  const switchToSignin = () => {
    playExpandingEffect();
    switchActive("signin");
  };

  const contextValue = {
    switchToSignup,
    switchToSignin,
    playExpandingEffect,
  };
  return (
    <div className={classes.modalRoot} ref={rootRef}>
      <Modal
        disablePortal
        disableEnforceFocus
        disableAutoFocus
        open
        aria-labelledby="server-modal-title"
        aria-describedby="server-modal-description"
        className={classes.modal}
        container={() => rootRef.current}
      >
        <div className={classes.paper}>
          <AccountContext.Provider
            className={classes.form}
            value={contextValue}
          >
            <div className={classes.boxContainer}>
              <div className={classes.topContainer}>
                {active === "signin"}
                {active === "signup"}
              </div>
              <div className={classes.InnerContainer}>
                {active === "signin" && <LoginForm />}
                {active === "signup" && <SignupForm />}
              </div>
            </div>
          </AccountContext.Provider>
        </div>
      </Modal>
    </div>
  );
}
