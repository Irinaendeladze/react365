import React from "react";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import useStyles from "../style";
import { cart, deleteItem, updateCart } from "../API";
import { Grid } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { toast } from "react-toastify";
import { notify } from "../Alert";

toast.configure();
export default function CartItem({
  description,
  title,
  image,
  price,
  id,
  qty = 1,
  setCartData,
}) {
  const classes = useStyles();

  const plus = (e) => {
    if (e.target.value === "plus") {
      updateCart(id, qty + 1).then((res) => {
        cart().then((data) => {
          setCartData(data);
        });
        console.log(res.cartItem.items);
      });
    }
  };

  const minus = (e) => {
    if (e.target.value === "minus") {
      updateCart(id, qty - 1).then((res) => {
        cart().then((data) => {
          setCartData(data);
        });
        console.log(res.cartItem.items);
      });
    }
  };

  const removeFromCart = (id) => {
    deleteItem(id).then((res) => {
      cart()
        .then((data) => {
          setCartData(data);
          notify(toast.success("Product delete from cart"));
        })

        .catch((err) => {
          alert("cart is empty");
        });
    });
  };

  return (
    <Grid item xs={4} md={6} lg={12} className={classes.cartContainer}>
      <TableContainer>
        <Table className={classes.cartContainer} aria-label="simple table">
          <TableRow>
            <TableCell align="right" className={classes.cartDescription}>
              {" "}
              {description}
            </TableCell>
            <TableCell align="right" className={classes.cartTitle}>
              {title}
            </TableCell>
            <TableCell align="right" className={classes.cartText}>
              <img src={image} alt="hello" className={classes.cartImage} />
            </TableCell>
            <TableCell align="right" className={classes.cartText}>
              {price}$
            </TableCell>
            <TableCell align="right" className={classes.cartText}>
              <div className="quantity">
                <button value={"plus"} onClick={plus}>
                  +
                </button>
                <button>{qty}</button>
                <button disabled={qty <= 1} value={"minus"} onClick={minus}>
                  -
                </button>
              </div>
            </TableCell>
            <TableCell align="right" className={classes.cartText}>
              <DeleteIcon
                className={classes.button}
                onClick={() => {
                  removeFromCart(id);
                }}
              />
            </TableCell>
          </TableRow>
        </Table>
      </TableContainer>
    </Grid>
  );
}
