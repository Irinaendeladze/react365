import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  AppBar: {
    backgroundColor: "rgba(36,41,62,.8)",
    padding: "0",
    margin: "0",
  },
  content: {
    display: "inline",
    height: "100%",
    objectFit: "cover",
    // maxWidth: "100%",
    // height: "100%",
  },
  headerButtons: {
    display: "flex",
    justifyContent: "space-evenly",
    fontWeight: 700,
    fontSize: "14px",
    lineHeight: "20px",
    color: "#fff",
    padding: "0 10px",
  },
  whitelogo: {
    display: "flex",
    justifyContent: "space-around",
    marginLeft: "80px",
    marginRight: "300px",
    padding: "10px",
  },
  Toolbar: {
    display: "flex",
    flexWrap: "nowrap",
    margin: "0 80px",
    padding: "10px",
  },
  fbIcon: {
    display: "flex",
    justifyContent: "space-around",
    marginTop: "12px",
  },
  signUpnow: {
    color: "#61d5df",
    fontWeight: 700,
    fontSize: "14px",
    lineHeight: "20px",
    border: "1px solid #61d5df ",
    "&:hover": {
      backgroundColor: "#61d5df",
      color: "#fff",
    },
  },
  loginButton: {
    color: "#61d5df",
    fontWeight: 700,
    fontSize: "14px",
    lineHeight: "20px",
  },
  wrapper: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    minWidth: "900px",
    width: "1000px",
    height: "500px",
    paddingBottom: "300px",
    "&:before": {
      boxSizing: "border-box",
    },
    marginTop: "300px",
    marginLeft: "230px",
  },
  wrapcontent: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
    backgroundColor: "rgba(36, 41, 62, 0.8)",
    flexDirection: "column",
    // minHeight: "0px",
    // minWidth: "0px",
    maxHeight: "370px",
    maxWidth: "1000px",
    transformOrigin: "50% 50%",
    opacity: "1",
    transform: "translate(0px, 0px)",
    visibility: "visible",
    padding: "0 150px",
  },
  titleone: {
    display: "flex",
    fontWeight: 500,
    fontSize: "30px",
    color: "#fff",
    margin: "0",
    fontFamily: "Gextra",
  },
  titlesecond: {
    display: "flex",
    fontWeight: 500,
    fontSize: "30px",
    fontFamily: "Gextra",
    color: "#fff",
    margin: "0 0 40px 0",
  },
  signupcenter: {
    backgroundColor: "#61d5df",
    fontSize: "16px",
    border: "none",
    background: "#61d5df",
    color: "#fff",
    padding: "15px 30px",
    marginTop: "20px",
    display: "block",
    maxWidth: "180px",
    borderRadius: "4px",
    margin: "0 auto",
    textAlign: "center",
  },
  background: {
    backgroundImage: `url(${firstpage})`,
    width: "1460px",
    height: "750px",
  },
  logomain: {
    margin: "40px 0 40px 0",
  },
  footer: {
    backgroundColor: "#49547d",
    height: "80px",
  },
}));
