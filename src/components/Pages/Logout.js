import { useHistory } from "react-router";
import useStyles from "../style";

const Logout = () => {
  const history = useHistory();
  const performLogout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    history.push("/");
  };
  const classes = useStyles();
  return (
    <input
      type="button"
      value="Logout"
      onClick={performLogout}
      className={classes.ProfileButton}
    />
  );
};

export default Logout;
