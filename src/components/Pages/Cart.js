import { useEffect, useState } from "react";
import { cart } from "../API";
import useStyles from "../style";
import LeftSidebar from "../sidebar/LeftSidebar";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import CartItem from "../Pages/CartItem";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

const Cart = () => {
  const [cartData, setCartData] = useState({});
  const [totalCart, setTotalCart] = useState(0);

  useEffect(() => {
    cart().then((result) => {
      setCartData(result);
      setTotalCart(result.cartItem.items);
      console.log(result.cartItem.items);
    });
  }, []);

  const classes = useStyles();
  return (
    <>
      <LeftSidebar />
      <div className={classes.root}>
        <AppBar position="static" elevation={0}>
          <Toolbar className={classes.Toolbar}>
            <Typography variant="h6" className={classes.title}>
              SHOPING CART - {cartData?.cartItem?.items.length}
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid item xs={4} md={6} lg={12} className={classes.itemDescription}>
          <TableHead>
            <TableRow className={classes.itemDescription}>
              <TableCell className={classes.descriptionText}>
                Item Description
              </TableCell>
              <TableCell className={classes.descriptionText} align="right">
                Title
              </TableCell>
              <TableCell align="right" className={classes.imageText}>
                Image
              </TableCell>
              <TableCell align="right" className={classes.descriptionText}>
                Price
              </TableCell>
              <TableCell align="right" className={classes.descriptionText}>
                QTY
              </TableCell>
            </TableRow>
          </TableHead>
        </Grid>

        <div className={cartData ? classes.cartProduct : classes.grid}>
          {cartData.cartItem &&
            cartData.cartItem.items.map((item) => (
              <CartItem
                description={item.description}
                id={item.id}
                title={item.title}
                qty={item.qty}
                key={item.id}
                price={item.price}
                image={item.image}
                setCartData={setCartData}
              />
            ))}
        </div>
      </div>
    </>
  );
};

export default Cart;
