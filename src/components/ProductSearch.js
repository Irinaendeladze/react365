import React, { useState } from "react";
import { VscSearch } from "react-icons/vsc";

const ProductSearch = ({ setInput }) => {
  const [searchInput, setSearchInput] = useState();

  const changeInput = (event) => {
    setSearchInput(event.target.value);
  };

  const enterSearch = (event) => {
    if (event.key === "Enter") {
      setInput(searchInput);
    }
  };

  return (
    <>
      <div id="searchButton">
        <input
          type="text"
          className="searchbox__input"
          id="searchbox__input"
          placeholder="Search..."
          value={searchInput}
          onChange={changeInput}
          onkeyDown={enterSearch}
        />
      </div>
      <button className="search-btn">
        <VscSearch
          size="18px"
          color="#49547d"
          searchInput={searchInput}
          setInput={setInput}
        />
      </button>
    </>
  );
};

export default ProductSearch;
