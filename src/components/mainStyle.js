import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    width: "82%",
    marginLeft: 317,
    paddingLeft: "90px",
    backgroundColor: "#f8f9fa",
    borderBottom: "1px solid #dbe0f3",
  },
  drawer: {
    width: 317,
    flexShrink: 0,
  },
  drawerSecond: {
    width: 50,
    flexShrink: 0,
  },
  drawerPaper: {
    width: 317,
    top: "0",
    flex: "1 0 auto",
    height: "100%",
    display: "flex",
    outline: "0",
    zIndex: "1200",
    position: "fixed",
    overflowY: "auto",
    flexDirection: "row",
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 2,
    backgroundColor: theme.palette.background.default,
    padding: "120px  30px 0 120px",
    marginLeft: "220px",
  },
}));
