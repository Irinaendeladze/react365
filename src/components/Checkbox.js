import React from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import useStyles from "./style";

const Checkboxs = ({ isChecked, onChecked }) => {
  const classes = useStyles();
  return (
    <FormControlLabel
      control={
        <Checkbox
          icon={<RadioButtonUncheckedIcon />}
          checkedIcon={<CheckCircleIcon />}
          className={classes.checkbox}
          type="checkbox"
          id="checkbox"
          checked={isChecked}
          onChange={onChecked}
          onClick={(e) => {
            e.stopPropagation();
            onChecked({ checked: !isChecked });
          }}
        />
      }
    />
  );
};

export default Checkboxs;
