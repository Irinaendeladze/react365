import Modal from "@material-ui/core/Modal";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { useEffect, useState } from "react";
import { getProducts, getSingleProduct } from "./API";
import * as yup from "yup";
import useStyles from "./style";
import { updateProduct } from "./API";
import { setProducts } from "./redax/actions/productActions";
import { toast } from "react-toastify";
import { notify } from "./Alert";
import { useDispatch } from "react-redux";
const addProductValidation = yup.object().shape({
  title: yup.string().min(2).max(25),
  description: yup.string().min(10).max(255),
  price: yup.number().integer().min(50),
  imageUrl: yup.string().url(),
});

toast.configure();
const EditProduct = (props) => {
  const { isOpen, onClose, productId } = props;
  const classes = useStyles();

  const dispatch = useDispatch();

  const [productValues, setProductValues] = useState({
    title: "",
    description: "",
    price: "",
    imageUrl: "",
  });

  useEffect(() => {
    if (productId) {
      getSingleProduct(productId).then((res) => {
        console.log("response", res);
        setProductValues({
          title: res.title,
          description: res.description,
          price: res.price,
          imageUrl: res.imageUrl,
        });
        setProducts(res);
      });
    }
  }, [productId]);

  const handleSubmit = (values) => {
    if (productId) {
      updateProduct(productId, values)
        .then((res) => {
          getProducts().then((res) => {
            dispatch({
              type: "EDIT_PRODUCT",
              payload: res,
            });
            notify(toast.success("Product updated"));
            localStorage.setItem("products", JSON.stringify(res));
          });
        })
        .catch((err) => alert(err.message));
    }
  };

  return (
    <Modal
      open={isOpen}
      onClose={onClose}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      className={classes.editProductModal}
    >
      <div className={classes.editAddPaper}>
        <Formik
          enableReinitialize
          initialValues={productValues}
          onSubmit={handleSubmit}
          validationSchema={addProductValidation}
        >
          {(props) => {
            return (
              <Form className={classes.addProduct}>
                <h4 className={classes.editAddProducTitle}>Edit Product </h4>

                <Field
                  placeholder="Title"
                  name="title"
                  className={classes.addProductField}
                />
                <ErrorMessage name="title" components="div" />
                <Field
                  placeholder="Description"
                  name="description"
                  components="textarea"
                  className={classes.addProductField}
                />
                <ErrorMessage name="description" components="div" />
                <Field
                  placeholder="Price"
                  name="price"
                  className={classes.addProductField}
                />
                <ErrorMessage name="price" components="div" />
                <Field
                  placeholder="ImageUrl"
                  name="imageUrl"
                  className={classes.addProductField}
                />
                <ErrorMessage name="imageUrl" components="div" />

                <button type="submit" className={classes.saveChangesBtn}>
                  {" "}
                  Save Changes
                </button>
              </Form>
            );
          }}
        </Formik>
      </div>
    </Modal>
  );
};

export default EditProduct;
