import React from "react";
import { addToCart } from "./API";

const Modal = ({ open, handleClose, modalData }) => {
  return (
    <div className="modal-backdrop">
      <div className="modal-content-wrapper" onClick={handleClose} open={open}>
        <div className="modal-item__right">
          <div className="item__info-price">
            <ul className="price__item">
              <li className="price__item-Rrp">
                <strong>214$</strong>
                <br />
                <span>RRP</span>
              </li>
              <li className="price__item-Cost">
                <strong>{modalData.price}$</strong>
                <br />
                <span>COST</span>
              </li>
              <li className="price__item-Profit">
                <strong>11 %(24$)</strong>
                <br />
                <span>PROFIT</span>
              </li>
            </ul>
          </div>
          <div className="modal__item-image">
            <img
              className="item__image"
              alt="product"
              src={modalData.imageUrl}
            />
          </div>
        </div>
        <div className="modal-item__left">
          <div className="modal__product" onClick={handleClose}>
            <div className="modal__close" />X
          </div>
          <h1 className="item_title">{modalData.title}</h1>
          <button
            title="Add to My Inventory"
            className="item__button"
            onClick={() => {
              addToCart(modalData.id, 1);
            }}
          >
            Add to My Inventory
          </button>
          <h3 className="item_productdesc">Product Description</h3>
          <div className="item_description">{modalData.description}</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
