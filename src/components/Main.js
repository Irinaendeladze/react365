import React from "react";
import mainStyle from "./mainStyle";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Header from "../components/Header/Header";
import Leftsidebar from "../components/sidebar/LeftSidebar";
import SubMenu from "../components/sidebar/SubMenu";
import { useState, useEffect } from "react";
import { useHistory } from "react-router";
import Catalog from "./Catalog";
import Modal from "./Modal";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { setProducts } from "./redax/actions/productActions";
import { Hidden } from "@material-ui/core";

const Main = ({ isOpen }) => {
  const [modalData, setModalData] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [open, setOpen] = useState(isOpen);
  const history = useHistory();
  const products = useSelector((state) => state.allProducts.products);

  const classes = mainStyle();
  const dispatch = useDispatch();

  useEffect(() => {
    setOpen(isOpen);
  }, [isOpen]);

  const handleOpen = (singleData) => {
    setOpenModal(true);
    setModalData(singleData);
  };

  const handleClose = () => {
    history.push("/catalog");
    setOpenModal(false);
  };

  const handleCheckProduct = (productId) => {
    const checkedProducts = products.map((product) =>
      product.id === productId
        ? { ...product, isChecked: !product.isChecked }
        : product
    );
    dispatch({
      type: "SELECTED_PRODUCTS",
      payload: checkedProducts,
    });
  };
  const selectedProducts = products.filter((product) => product.isChecked);
  useEffect(() => {
    setProducts(selectedProducts);
  }, [products]);

  const handleClearAll = () => {
    dispatch({
      type: " HANDLE_CLEAR_ALL",
      payload: JSON.parse(localStorage.getItem("products")).map((product) => ({
        ...product,
        isChecked: false,
      })),
    });
  };

  const handleSelectAll = () => {
    dispatch({
      type: "HANDLE_SELECT_ALL",
      payload: JSON.parse(localStorage.getItem("products")).map((product) => ({
        ...product,
        isChecked: true,
      })),
    });
  };

  return (
    <>
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar} elevation={0}>
          <Toolbar>
            <Typography variant="h6" noWrap>
              <Header
                selectedProducts={selectedProducts.length}
                handleClearAll={handleClearAll}
                handleSelectAll={handleSelectAll}
              />
            </Typography>
          </Toolbar>
        </AppBar>
        <Hidden smDown>
          <Drawer
            className={classes.drawerWidth}
            variant="permanent"
            classes={{
              paper: classes.drawerPaper,
            }}
            anchor="left"
          >
            <Leftsidebar />
            <SubMenu />
          </Drawer>{" "}
        </Hidden>
        <div className={classes.toolbar} />
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Catalog
            handleClose={handleClose}
            handleOpen={handleOpen}
            onChecked={handleCheckProduct}
          />

          {openModal && (
            <Modal
              modalData={modalData}
              handleClose={handleClose}
              isOpen={modalData.id}
              open={open}
            />
          )}
        </main>
      </div>
    </>
  );
};
export default Main;
