import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./components/redax/store";

import { createMuiTheme, ThemeProvider } from "@material-ui/core";

const themeDropShip = createMuiTheme({
  palette: {
    primary: {
      main: "#61d5df",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ff7961",
      main: "#61d5df",
      dark: "#ba000d",
      contrastText: "#000",
    },
  },
  typography: {
    h6: {
      fontWeight: 350,
      fontSize: "13px",
    },
  },
  overrides: {
    MuiSlider: {
      thumb: {
        color: "#49547d",
      },
      track: {
        color: "#49547d",
      },
      rail: {
        color: "black",
      },
    },
  },
});

console.log(themeDropShip);
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={themeDropShip}>
        <Router>
          <App />
        </Router>
      </ThemeProvider>
    </Provider>
  </React.StrictMode>,

  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
