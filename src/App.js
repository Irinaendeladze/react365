import "./App.css";
import LeftSidebar from "./components/sidebar/LeftSidebar";
import Main from "./components/Main";
import React from "react";
import Profile from "./components/Pages/Profile";
import Dashboard from "./components/Pages/Dashboard";
import Inventory from "./components/Pages/Inventory";
import Cart from "./components/Pages/Cart";
import Orders from "./components/Pages/Orders";
import { Route, Switch } from "react-router-dom";
import AddProduct from "./components/Pages/AddProduct";
import FirstPage from "./components/Pages/FirstPage";
import Landing from "./components/Pages/Landing";
import EditProduct from "./components/EditProduct";

function App() {
  return (
    <body>
      <div className="container">
        <Switch>
          <Route exact path="/">
            <FirstPage />
          </Route>
          <Route path="/landing">
            <Landing />
          </Route>
          <Route path={"/profile"}>
            <Profile />
          </Route>
          <Route path={"/dashboard"}>
            <Dashboard />
          </Route>
          <Route path={"/catalog"}>
            <Main />
          </Route>
          <Route path={"/inventory"}>
            <Inventory />
          </Route>
          <Route path={"/cart"}>
            <Cart />
          </Route>
          <Route path={"/orders"}>
            <Orders />
          </Route>
          <Route path={"/editProduct"}>
            <EditProduct />
          </Route>
          <Route path={"/getProducts/:productId?"}>
            <LeftSidebar />
            <AddProduct />
          </Route>
        </Switch>
      </div>
    </body>
  );
}

export default App;
